package tkocsis.akka.example;

import com.rethinkdb.RethinkDB;
import com.rethinkdb.net.Cursor;

public class RethinkListening {
	
	public static final RethinkDB r = RethinkDB.r;
	
	public static void main(String[] args) {
		com.rethinkdb.net.Connection conn = r.connection().hostname("localhost").port(28015).connect();
		Cursor changeCursor = r.db("test").table("lpr").changes().run(conn);
		changeCursor.forEach(i -> {
			System.out.println(i);
		});
	}
}
