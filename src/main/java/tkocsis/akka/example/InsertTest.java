package tkocsis.akka.example;

import java.sql.Connection;
import java.time.LocalDateTime;
import java.util.UUID;

import org.bson.Document;
import org.jooq.DSLContext;

import com.couchbase.client.java.document.JsonDocument;
import com.couchbase.client.java.document.json.JsonObject;
import com.datastax.driver.core.Session;
import com.rethinkdb.RethinkDB;

import tkocsis.akka.LprUtils;
import tkocsis.akka.lpr.db.tables.Licenseplates;
import tkocsis.akka.lpr.db.tables.records.LicenseplatesRecord;
import tkocsis.akka.lpr.persist.CassandraConnector;
import tkocsis.akka.lpr.persist.CouchbaseConnectionProvider;
import tkocsis.akka.lpr.persist.MongoDBConnection;
import tkocsis.akka.lpr.persist.MysqlConnectionProvider;

public class InsertTest {

	public static final int AMOUNT = 2_000_000;
	
	public static void main(String[] args) {
//		cassandraInsert();
//		jooqInsert();
//		couchbaseInsert();
//		mongoInsert(AMOUNT * 5);
		rethinkDbInsert(AMOUNT * 5);
	}
	
	/**
	 * 
	 * CREATE KEYSPACE vidux WITH replication = { 'class' : 'SimpleStrategy', 'replication_factor' : 3 };
	 * CREATE TABLE plates (plate text PRIMARY KEY, timestamp timestamp)
	 * 
	 */
	public static void cassandraInsert() {
		Session session = CassandraConnector.getSession();
		long now = System.currentTimeMillis();
		System.out.println("Start cassandra: " + LocalDateTime.now());
		for (int i = 0; i < AMOUNT; i++) {
			if (i % 100000 == 0) {
				System.out.println(LocalDateTime.now() + " " + i);
			}
			session.execute("INSERT INTO vidux.plates (plate, timestamp) VALUES (?, ?)", LprUtils.randomLicense(), System.currentTimeMillis());
		}
		long runningTime = System.currentTimeMillis() - now;
		System.out.println("End cassandra: " + LocalDateTime.now() + " - " + (runningTime) + " ms");
	}
	
	/**
	 * CREATE TABLE IF NOT EXISTS `licenseplates` (
		  `id` int(11) NOT NULL,
		  `plate` text NOT NULL,
		  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
		) ENGINE=InnoDB AUTO_INCREMENT=2047870 DEFAULT CHARSET=utf8;
	 */
	public static void jooqInsert() {
		long now = System.currentTimeMillis();
		System.out.println("Start JOOQ: " + LocalDateTime.now());
		try (Connection conn = MysqlConnectionProvider.getConnection()) {
			DSLContext context = MysqlConnectionProvider.getContext(conn);
			for (int i = 0; i < AMOUNT; i++) {
				if (i % 100000 == 0) {
					System.out.println(LocalDateTime.now() + " " + i);
				}
				LicenseplatesRecord licensePlateRecord = context.newRecord(Licenseplates.LICENSEPLATES);
				licensePlateRecord.setPlate(LprUtils.randomLicense());
				licensePlateRecord.store();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		long runningTime = System.currentTimeMillis() - now;
		System.out.println("End JOOQ: " + LocalDateTime.now() + " - " + (runningTime) + " ms");
	}
	
	
	
	public static void couchbaseInsert() {
		long now = System.currentTimeMillis();
		System.out.println("Start Couchbase: " + LocalDateTime.now());
		
		for (int i = 0; i < AMOUNT; i++) {
			if (i % 100000 == 0) {
				System.out.println(LocalDateTime.now() + " " + i);
			}
			JsonObject jsonObject = JsonObject.create();
			jsonObject.put("plate", LprUtils.randomLicense());
			jsonObject.put("timestamp", System.currentTimeMillis());
			JsonDocument document = JsonDocument.create("plate-" + UUID.randomUUID(), jsonObject);
			CouchbaseConnectionProvider.getBucket().insert(document);
		}
		
		long runningTime = System.currentTimeMillis() - now;
		System.out.println("End Couchbase: " + LocalDateTime.now() + " - " + (runningTime) + " ms");
	}
	
	
	/**
	 * use vidux
	 * db.createCollection(plates)
	 */
	public static void mongoInsert(int amount) {
		long now = System.currentTimeMillis();
		System.out.println("Start mongoInsert: " + LocalDateTime.now());
		
		for (int i = 0; i < amount; i++) {
			if (i % 100000 == 0) {
				System.out.println(LocalDateTime.now() + " " + i);
			}
			Document doc = new Document("plate", LprUtils.randomLicense()).append("timestamp", System.currentTimeMillis());
			MongoDBConnection.getLprCollection().insertOne(doc);
		}
		
		long runningTime = System.currentTimeMillis() - now;
		System.out.println("End mongoInsert: " + LocalDateTime.now() + " - " + (runningTime) + " ms");
	}
	
	public static final RethinkDB r = RethinkDB.r;
	
	public static void rethinkDbInsert(int amount) {
		com.rethinkdb.net.Connection conn = r.connection().hostname("localhost").port(28015).connect();
		r.db("test");
		try {
			r.db("test").tableDrop("lpr").run(conn);
		} catch (Exception e) {
			System.out.println("LPR table not exists yet");
		}
		r.tableCreate("lpr").run(conn);
		
		long now = System.currentTimeMillis();
		System.out.println("Start rethinkInsert: " + LocalDateTime.now());
		for (int i = 0; i < amount; i++) {
			if (i % 100000 == 0) {
				System.out.println(LocalDateTime.now() + " " + i);
			}
			r.table("lpr").insert(r.hashMap("plate", LprUtils.randomLicense()).with("timestamp", System.currentTimeMillis())).run(conn);
		}
		
		long runningTime = System.currentTimeMillis() - now;
		System.out.println("End rethinkInsert: " + LocalDateTime.now() + " - " + (runningTime) + " ms");
	}
}
