package tkocsis.akka.example;

import java.time.LocalDateTime;

import com.datastax.driver.core.Session;

import tkocsis.akka.LprUtils;
import tkocsis.akka.lpr.persist.CassandraConnector;

public class CassandraTest {

	public static void main(String[] args) {
		Session session = CassandraConnector.getSession();
		
		System.out.println(LocalDateTime.now());
		for (int i = 0; i < 10000000; i++) {
			session.execute("INSERT INTO vidux.plates (plate, timestamp) VALUES (?, ?)", LprUtils.randomLicense(), System.currentTimeMillis());
		}
		System.out.println(LocalDateTime.now());
	}
	
	
}
