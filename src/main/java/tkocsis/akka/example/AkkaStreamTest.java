package tkocsis.akka.example;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletionStage;

import akka.Done;
import akka.NotUsed;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Status;
import akka.stream.ActorMaterializer;
import akka.stream.ClosedShape;
import akka.stream.FlowShape;
import akka.stream.Graph;
import akka.stream.OverflowStrategy;
import akka.stream.SinkShape;
import akka.stream.SourceShape;
import akka.stream.UniformFanOutShape;
import akka.stream.javadsl.Broadcast;
import akka.stream.javadsl.Flow;
import akka.stream.javadsl.GraphDSL;
import akka.stream.javadsl.RunnableGraph;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;

public class AkkaStreamTest {

	ActorSystem actorSystem;
	ActorMaterializer mat;
	
	public AkkaStreamTest() {
		actorSystem = ActorSystem.create("akka-stream-test");
		mat = ActorMaterializer.create(actorSystem);
		
		basicUsage();
		basicProcess();
		basicProcessSubset();
		basicProcessReusedParts();
		basicGraph();
		actorSource();
	}
	
	public void basicUsage() {
		List<String> inputStringList = Arrays.asList("a", "b", "c", "d");
		
		Source<String, NotUsed> streamSource = Source.from(inputStringList);
		
		CompletionStage<Done> streamState = streamSource.runForeach(i -> System.err.println(i), mat);
		streamState.thenAccept(done -> System.err.println("Done"));
		
		// second run possible
		streamSource.runForeach(i -> System.err.println("Second run: " + i), mat);
	}
	
	public void basicProcess() {
		Source<Integer, NotUsed> streamSource = Source.range(0, 100);
		streamSource
			.map(i -> i * i)
			.runForeach(i -> System.err.println(i), mat);
	}
	
	public void basicProcessSubset() {
		Source<Integer, NotUsed> streamSource = Source.range(0, 100);
		Source<Integer, NotUsed> subset = streamSource.filter(i -> i % 2 == 0);
		subset.runForeach(i -> System.err.println(i), mat);
	}
	
	public void basicProcessReusedParts() {
		Source<Integer, NotUsed> streamSource = Source.range(0, 100);
		
		Flow<Integer, Integer, NotUsed> calculateEverything = 
				Flow.of(Integer.class)
					.filter(i -> i % 2 == 0)
					.map(i -> i * i)
					.map(i -> ++i);
		
		Flow<Integer, String, NotUsed> converter = Flow.of(Integer.class).map(i -> "Convert to string " + i);
		
		streamSource
			.via(calculateEverything)
			.via(converter)
			.runForeach(i -> System.err.println(i), mat);
	}
	
	public void basicGraph() {
		Source<Integer, NotUsed> streamSource = Source.range(0, 100);
		
		Graph<ClosedShape, NotUsed> graph = GraphDSL.create(builder -> {
			final UniformFanOutShape<Integer, Integer> bcast = builder.add(Broadcast.create(2));
			FlowShape<Integer, Integer> filter2 = builder.add(Flow.of(Integer.class).filter(i -> i % 2 == 0));
			FlowShape<Integer, Integer> filter3 = builder.add(Flow.of(Integer.class).filter(i -> i % 3 == 0));
			
			SinkShape<Object> print2 = builder.add(Sink.foreach(i -> System.err.println("Paros szamok: " + i)));
			SinkShape<Object> print3 = builder.add(Sink.foreach(i -> System.err.println("Harommal oszthato szamok: " + i)));
			
			SourceShape<Integer> source = builder.add(streamSource);
			
			builder.from(source).viaFanOut(bcast).via(filter2).to(print2);
			builder.from(bcast).via(filter3).to(print3);
			
			return ClosedShape.getInstance();
		});
		RunnableGraph.fromGraph(graph).run(mat);
	}
	
	public void actorSource() {
		Source<String, ActorRef> actorRefSource = Source.<String> actorRef(10, OverflowStrategy.fail());
		actorRefSource
			.mapMaterializedValue(actorRef -> {
				actorRef.tell("Hello1", ActorRef.noSender());
				actorRef.tell("Hello2", ActorRef.noSender());
				actorRef.tell("Hello3", ActorRef.noSender());
				actorRef.tell(new Status.Success(0), ActorRef.noSender());
				return NotUsed.getInstance();
			})
			.runForeach(i -> System.err.println(i), mat)
			.handle((result, ex) -> {
				System.err.println(result);
				return NotUsed.getInstance();
			});
	}
	
	public static void main(String[] args) {
		new AkkaStreamTest();
	}
}

