package tkocsis.akka.firstapp;

import java.util.Random;

import akka.dispatch.Futures;
import akka.japi.Option;
import scala.concurrent.Future;
import tkocsis.akka.Log;

public class LprProcessorTypedActor {

	
	public static class LprStatistics {
		String plate;
		int occurs;
		
		public LprStatistics(String plate, int occurs) {
			this.plate = plate;
			this.occurs = occurs;
		}
	}
	
	public static interface LprProcessor {
		public Future<LprStatistics> getLprStatistics(String plate);
		
		public Option<LprStatistics> getLprStatisticsNow(String plate); // blocking like LprStatistics return
	}
	
	static class LprProcessorImpl implements LprProcessor {
		
		public LprProcessorImpl() {
		}
		
		@Override
		public Future<LprStatistics> getLprStatistics(String plate) {
			Log.log("Start processing " + plate);
			Random r = new Random();
			try {
				Thread.sleep(r.nextInt(1500));
			} catch (InterruptedException e) {
				return Futures.failed(e);
			} // simulating process
			
			return Futures.successful(new LprStatistics(plate, r.nextInt(1000)));
		}
		
		@Override
		public Option<LprStatistics> getLprStatisticsNow(String plate) {
			Log.log("Start processing " + plate);
			Random r = new Random();
			try {
				Thread.sleep(r.nextInt(1000));
			} catch (InterruptedException e) {
				return Option.none(); 
			} // simulating process
			
			return Option.option(new LprStatistics(plate, r.nextInt(1000)));
		}
		
	}
	
}
