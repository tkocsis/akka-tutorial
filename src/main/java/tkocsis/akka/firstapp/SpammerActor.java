package tkocsis.akka.firstapp;

import java.util.concurrent.Callable;

import akka.actor.UntypedActor;
import akka.dispatch.Futures;
import akka.dispatch.OnSuccess;
import scala.concurrent.Future;
import tkocsis.akka.Log;

public class SpammerActor extends UntypedActor {
	
	private int interval = 0;
	private SpammerActor(int interval) {
		this.interval = interval;
	}

	@Override
	public void preStart() throws Exception {
		super.preStart();
		Log.log("PreStart: Starting spammer actor with interval: " + interval);
		
		Future<String> future = Futures.future(new Callable<String>() {
			
			@Override
			public String call() throws Exception {
				Log.log("start wait");
				Thread.sleep(2000);
				Log.log("End wait");
				return "FUCK";
			}
			
		}, getContext().dispatcher());
		future.onSuccess(new OnSuccess<String>() {
			
			@Override
			public void onSuccess(String result) throws Throwable {
				Log.log("Onsuccess called: " + result);
				getSelf().tell(result, getSelf());
			}
		}, getContext().dispatcher());
		
//		String result = (String) Await.result(future, Duration.create(1, TimeUnit.MINUTES));
//		Log.log("Result arrived: " + result);
	}
	
	@Override
	public void postStop() throws Exception {
		// TODO Auto-generated method stub
		super.postStop();
	}
	
	@Override
	public void onReceive(Object message) throws Throwable {
		if (message instanceof String) {
			Log.log("Received message '" + message + "' from " + getSender());
			if (getSender() != getSelf())
				getSender().tell("No, fuck you " + getSender() + "!", getSelf());
		} else {
			unhandled(message);
		}
	}

}
