package tkocsis.akka.firstapp;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.TypedActor;
import akka.actor.TypedProps;
import akka.dispatch.OnSuccess;
import akka.japi.Option;
import akka.routing.RoundRobinGroup;
import akka.util.Timeout;
import scala.concurrent.Future;
import tkocsis.akka.Log;
import tkocsis.akka.firstapp.LprProcessorTypedActor.LprProcessor;
import tkocsis.akka.firstapp.LprProcessorTypedActor.LprProcessorImpl;
import tkocsis.akka.firstapp.LprProcessorTypedActor.LprStatistics;

public class TypedActorExample {

	public static void run(ActorSystem system) {
		LprProcessor typedProcessor = 
				TypedActor
					.get(system)
					.typedActorOf(
						new TypedProps<LprProcessorImpl>(LprProcessor.class, LprProcessorImpl.class)
							.withTimeout(new Timeout(15000, TimeUnit.MILLISECONDS)));
		
		
		for (int i = 0; i < 20; i++) {
			Option<LprStatistics> stat = typedProcessor.getLprStatisticsNow("KUU-734");
			if (stat.isDefined()) { 
				Log.log("Answer arrived: " + stat.get().plate);
			} else {
				Log.log("Answer not arrived");
			}
		}
		
		for (int i = 0; i < 20; i++) {
			int j = i;
			Future<LprStatistics> statistics = typedProcessor.getLprStatistics("KUU-734");
			statistics.onSuccess(new OnSuccess<LprStatistics>() {
				
				@Override
				public void onSuccess(LprStatistics statistics) throws Throwable {
					Log.log("Processed (" + j + "): " + statistics.plate);
				}
			}, system.dispatcher());
		}
		Log.log("Second for ended.");
	}
	
	public static void runMulti(ActorSystem system) {
		Set<LprProcessor> processors = new HashSet<>();
		Set<String> routePaths = new HashSet<>();
		
		for (int i = 0; i < 15; i++) {
			LprProcessor typedProcessor = 
				TypedActor
					.get(system)
					.typedActorOf(
						new TypedProps<LprProcessorImpl>(LprProcessor.class, LprProcessorImpl.class)
							.withTimeout(new Timeout(15000, TimeUnit.MILLISECONDS)));
			
			processors.add(typedProcessor);
			routePaths.add(TypedActor.get(system).getActorRefFor(typedProcessor).path().toStringWithoutAddress());
			Log.log(TypedActor.get(system).getActorRefFor(typedProcessor).path() + "");
			Log.log(TypedActor.get(system).getActorRefFor(typedProcessor).path().toStringWithoutAddress() + "");
			
		}
		
		ActorRef routerActor = system.actorOf(new RoundRobinGroup(routePaths).props(), "router");
		LprProcessor routerProcessor = TypedActor.get(system)
				.typedActorOf(new TypedProps<LprProcessorImpl>(LprProcessor.class, LprProcessorImpl.class), routerActor);
		
		for (int i = 0; i < 20; i++) {
			int j = i;
			Future<LprStatistics> statistics = routerProcessor.getLprStatistics("KUU-734");
			statistics.onSuccess(new OnSuccess<LprStatistics>() {
				
				@Override
				public void onSuccess(LprStatistics statistics) throws Throwable {
					Log.log("Processed (" + j + "): " + statistics.plate);
				}
			}, system.dispatcher());
		}
		
	}
}
