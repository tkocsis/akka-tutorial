package tkocsis.akka.firstapp;

import akka.actor.ActorSystem;
import akka.actor.Props;

public class Main {

	public static void main(String[] args) {
		ActorSystem system = ActorSystem.create("first-app");
		
		system.actorOf(Props.create(LprDetectActor.class), "lpr-detect");
		system.actorOf(Props.create(LprListenerActor.class), "lpr-listener-1");
		system.actorOf(Props.create(LprListenerActor.class), "lpr-listener-2");
		system.actorOf(Props.create(LprListenerActor.class), "lpr-listener-3");
		system.actorOf(Props.create(LprListenerActor.class), "lpr-listener-4");
		
		// typed
		TypedActorExample.run(system);
//		TypedActorExample.runMulti(system);
	}
}
