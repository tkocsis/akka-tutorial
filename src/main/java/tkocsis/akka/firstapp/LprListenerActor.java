package tkocsis.akka.firstapp;

import akka.actor.UntypedActor;
import tkocsis.akka.Log;
import tkocsis.akka.firstapp.LprDetectActor.LprMessage;

public class LprListenerActor extends UntypedActor {

	@Override
	public void onReceive(Object message) throws Throwable {
		if (message instanceof LprMessage) {
			Log.log("{" + getSelf().path() + "} Lpr received: " + message);
		} else {
			unhandled(message);
		}
	}
	
}
