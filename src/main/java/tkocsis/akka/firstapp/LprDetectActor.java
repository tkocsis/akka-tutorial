package tkocsis.akka.firstapp;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import akka.actor.ActorRef;
import akka.actor.ActorSelection;
import akka.actor.Address;
import akka.actor.Cancellable;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.cluster.Cluster;
import akka.cluster.MemberStatus;
import akka.pattern.Patterns;
import akka.cluster.ClusterEvent.MemberEvent;
import akka.cluster.ClusterEvent.MemberUp;
import akka.routing.ActorRefRoutee;
import akka.routing.RoundRobinRoutingLogic;
import akka.routing.Routee;
import akka.routing.Router;
import scala.concurrent.duration.Duration;
import tkocsis.akka.LprUtils;

public class LprDetectActor extends UntypedActor {

	public static class LprMessage implements Serializable {
		private static final long serialVersionUID = 1L;
		
		final String lpr;

		public LprMessage(String lpr) {
			this.lpr = lpr;
		}

		@Override
		public String toString() {
			return lpr;
		}

		public static LprMessage generate() {
			return new LprMessage(LprUtils.randomLicense());
		}
	}

	private Cancellable timer;
	
	private Set<ActorRef> saveActors = new HashSet<>();
	private Set<ActorSelection> listeningActors = new HashSet<>();
	
	private Router router;
	
	private Cluster cluster = Cluster.get(getContext().system());
	
	public LprDetectActor() {
		// create some actor to save detected lprs to database
		List<Routee> routees = new ArrayList<>();
		for (int i = 0; i < 5; i++) {
			ActorRef child = getContext().actorOf(Props.create(LprPersisterActor.class), "lpr-persister" + i);
			saveActors.add(child);
			routees.add(new ActorRefRoutee(child));
		}
		router = new Router(new RoundRobinRoutingLogic(), routees);
		cluster.subscribe(getSelf(), MemberEvent.class);
		
		// start generate LPRs
		timer = getContext().system().scheduler().schedule(
					Duration.create(0, TimeUnit.MILLISECONDS),
					Duration.create(1000, TimeUnit.MILLISECONDS), 
					() -> {
						LprMessage detectedPlate = LprMessage.generate();
						listeningActors.stream().forEach(actorRef -> actorRef.tell(detectedPlate, getSelf()));
	
//						saveActors.stream()
//							.forEach(ref -> ref.tell(detectedPlate, getSelf()));
						
//						saveActors.stream().forEach(ref -> {
//	
//							final Future<Object> f = Patterns.ask(ref, detectedPlate, 5000);
//							f.onSuccess(new OnSuccess<Object>() {
//								
//								@Override
//								public void onSuccess(Object arg0) throws Throwable {
//									Log.log("Finished: " + arg0);
//									getSelf().tell("Finished", ref);								
//								}
//							}, getContext().system().dispatcher());
//							f.onFailure(new OnFailure() {
//								
//								@Override
//								public void onFailure(Throwable arg0) throws Throwable {
//									Log.log("Warning, no response arrived for message " + detectedPlate);
//								}
//							}, getContext().system().dispatcher());
//						});
						
						router.route(detectedPlate, getSelf());
					}, getContext().dispatcher());
	}

	@Override
	public void postStop() throws Exception {
		super.postStop();
		timer.cancel();
	}

	@Override
	public void onReceive(Object message) throws Throwable {
		
		if (message instanceof MemberUp) {
			MemberUp mUp = (MemberUp) message;
			Address memberAddress = mUp.member().address();
			ActorSelection actorSelection = getContext().actorSelection(memberAddress + "/user/lpr-listener*");
			listeningActors.add(actorSelection);
		}
		unhandled(message);
	}
}
