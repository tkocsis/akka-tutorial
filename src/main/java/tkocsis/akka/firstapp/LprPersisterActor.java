package tkocsis.akka.firstapp;

import akka.actor.UntypedActor;
import tkocsis.akka.Log;
import tkocsis.akka.firstapp.LprDetectActor.LprMessage;

public class LprPersisterActor extends UntypedActor {

	@Override
	public void onReceive(Object message) throws Throwable {
		if (message instanceof LprMessage) {
			Log.log("{" + getSelf().path() + "} Saving plate: " + message);
			getSender().tell("Ok", getSelf());
		} else {
			unhandled(message);
		}
	}

}
