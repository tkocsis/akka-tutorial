package tkocsis.akka.lpr;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.cluster.pubsub.DistributedPubSub;
import akka.cluster.pubsub.DistributedPubSubMediator;
import akka.japi.pf.ReceiveBuilder;
import scala.concurrent.duration.Duration;
import scala.concurrent.duration.FiniteDuration;
import tkocsis.akka.LprUtils;
import tkocsis.akka.lpr.message.DetectedPlate;
import tkocsis.akka.lpr.persist.LprPersister;

public class LprDetectActor extends AbstractActor {
	
	LprPersister lprPersister; 
	ActorRef mediator;

	public LprDetectActor(LprPersister lprPersister) {
		this.lprPersister = lprPersister;
		
		receive(ReceiveBuilder
				.match(DetectedPlate.class, this::onReceiveDetectedPlate)
				.matchAny(message -> unhandled(message))
				.build());
		
		// get system
		ActorSystem system = context().system();
		
		mediator = DistributedPubSub.get(system).mediator();
		
//		system.scheduler().schedule(
//				Duration.create(0, TimeUnit.SECONDS),
//				Duration.create(1, TimeUnit.SECONDS),
//				() -> {
//					String plate = LprUtils.randomLicense();
//					self().tell(DetectedPlate.of(plate), ActorRef.noSender());
//				},
//				system.dispatcher());
		
		scheduleNextPlateDetection();
	}
	
	private void scheduleNextPlateDetection() {
		Random r = new Random();
		FiniteDuration firstRun = Duration.create(r.nextInt(20), TimeUnit.MILLISECONDS);
		context().system().scheduler().scheduleOnce(
			firstRun,
			() ->  {
				String plate = LprUtils.randomLicense();
				self().tell(DetectedPlate.of(plate), ActorRef.noSender());
				scheduleNextPlateDetection();
			},
			context().system().dispatcher());
	}
	
	public void onReceiveDetectedPlate(DetectedPlate plate) {
		lprPersister.saveLpr(plate);
		mediator.tell(new DistributedPubSubMediator.Publish("lpr.detected", plate), self());
	}
	
}
