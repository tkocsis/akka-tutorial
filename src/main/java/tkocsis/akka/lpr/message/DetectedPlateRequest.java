package tkocsis.akka.lpr.message;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;

public class DetectedPlateRequest {

	final CompletableFuture<Optional<Object>> future;
	
	public DetectedPlateRequest(CompletableFuture<Optional<Object>> future) {
		this.future = future;
	}
	
	public CompletableFuture<Optional<Object>> getFuture() {
		return future;
	}
}
