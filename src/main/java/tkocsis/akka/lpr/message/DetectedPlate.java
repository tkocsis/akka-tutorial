package tkocsis.akka.lpr.message;

public class DetectedPlate {

	final String plate;
	final long timestamp;
	
	public static DetectedPlate of(String plate) {
		return new DetectedPlate(plate);
	}
	
	private DetectedPlate(String plate) {
		this.plate = plate;
		timestamp = System.currentTimeMillis();
	}
	
	public String getPlate() {
		return plate;
	}
	
	public long getTimestamp() {
		return timestamp;
	}
	
	@Override
	public String toString() {
		return "DetectedPlate: " + plate;
	}
}
