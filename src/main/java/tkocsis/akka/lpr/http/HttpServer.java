package tkocsis.akka.lpr.http;

import java.util.concurrent.CompletableFuture;

import org.json.JSONObject;

import akka.NotUsed;
import akka.actor.ActorSystem;
import akka.http.javadsl.ConnectHttp;
import akka.http.javadsl.Http;
import akka.http.javadsl.marshallers.jackson.Jackson;
import akka.http.javadsl.model.ContentTypes;
import akka.http.javadsl.model.HttpEntities;
import akka.http.javadsl.model.HttpRequest;
import akka.http.javadsl.model.HttpResponse;
import akka.http.javadsl.server.AllDirectives;
import akka.http.javadsl.server.Route;
import akka.http.javadsl.unmarshalling.Unmarshaller;
import akka.stream.ActorMaterializer;
import akka.stream.javadsl.Flow;
import akka.stream.javadsl.Source;
import akka.util.ByteString;
import tkocsis.akka.lpr.LprLiveFeedSource;
import tkocsis.akka.lpr.graphql.GraphQLService;

public class HttpServer {
	
	static class GrahpQLDataQuery {
		String query;
		String variables;
		
		public GrahpQLDataQuery() {
		}
		
		public GrahpQLDataQuery(String data, String variables) {
			this.query = data;
			this.variables = variables;
		}
		public void setQuery(String query) {
			this.query = query;
		}
		public String getQuery() {
			return query;
		}
		
		public String getVariables() {
			return variables;
		}
		public void setVariables(String variables) {
			this.variables = variables;
		}
	}
	
	private class LprHttpRoutes extends AllDirectives {
		public Route createRoutes() {
			return route(
				path("api", () -> route(
					get(() -> complete("aaa")),
					post(() -> complete("bbb"))
				)),
				path("lprstream", () -> route(
					get(() -> {
						Source<Object, NotUsed> plates = Source.unfoldResourceAsync(
								  () ->  {
									  CompletableFuture<LprLiveFeedSource> fut = new CompletableFuture<>();
									  fut.complete(new LprLiveFeedSource(actorSystem));
									  return fut;
								  },
								  liveFeed -> liveFeed.nextPlate(),
								  reader -> reader.close());
						
						return complete(
								HttpEntities.create(
									ContentTypes.TEXT_PLAIN_UTF8,
									plates.map(x -> ByteString.fromString(x + "\n"))
								)
							); 
					})
				)),
				path("graphql", () -> route(
					get(() -> {
						return complete("Teszt");
					}),
					post(() -> entity(Jackson.unmarshaller(GrahpQLDataQuery.class), graphQuery -> {
						JSONObject object = new JSONObject();
						object.put("data", new GraphQLService().query(graphQuery.getQuery()));
						return complete(object.toString());							
					}))
				)),
				path("teszt", () -> route(
						get(() -> {
							return complete("Teszt");
						}),
						post(() -> entity(Unmarshaller.entityToString(), str -> {
							System.out.println(str);
							return complete(str);							
						}))
					))
			);
		}
		
	}
	
	private ActorSystem actorSystem;
	
	public HttpServer(ActorSystem actorSystem) {
		this.actorSystem = actorSystem;
		
		Http http = Http.get(actorSystem);
		ActorMaterializer materializer = ActorMaterializer.create(actorSystem);
		
		LprHttpRoutes routes = new LprHttpRoutes();
		Flow<HttpRequest, HttpResponse, NotUsed> flow = routes.createRoutes().flow(actorSystem, materializer);
		http.bindAndHandle(flow, ConnectHttp.toHost("10.30.0.80", 8080), materializer);
	}
	
}
