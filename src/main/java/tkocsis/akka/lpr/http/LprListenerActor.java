package tkocsis.akka.lpr.http;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.PoisonPill;
import akka.cluster.pubsub.DistributedPubSub;
import akka.cluster.pubsub.DistributedPubSubMediator;
import akka.japi.pf.ReceiveBuilder;
import tkocsis.akka.lpr.message.DetectedPlate;
import tkocsis.akka.lpr.message.DetectedPlateRequest;

public class LprListenerActor extends AbstractActor {

	ActorRef mediator;
	
	List<DetectedPlate> detectedPlates = new ArrayList<>();
	CompletableFuture<Optional<Object>> request = null;
	
	public LprListenerActor() {
		receive(ReceiveBuilder
				.match(DetectedPlate.class, this::onReceiveDetectedPlate)
				.match(DetectedPlateRequest.class, this::onReceiveDetectedPlateRequest)
				.match(PoisonPill.class, this::onReceivePoisonPill)
				.build());
		
		mediator = DistributedPubSub.get(context().system()).mediator();
		mediator.tell(new DistributedPubSubMediator.Subscribe("lpr.detected", self()), self());
	}
	
	public void onReceiveDetectedPlate(DetectedPlate plate) {
		detectedPlates.add(plate);
		
		if (request != null) {
			request.complete(Optional.of(detectedPlates.remove(0).getPlate()));
			request = null;
		}
	}
	
	public void onReceiveDetectedPlateRequest(DetectedPlateRequest plateRequest) {
		request = plateRequest.getFuture();
		
		if (detectedPlates.size() > 0) {
			request.complete(Optional.of(detectedPlates.remove(0).getPlate()));
			request = null;
		}
	}
	
	public void onReceivePoisonPill(PoisonPill pill) {
		mediator.tell(new DistributedPubSubMediator.Unsubscribe("lpr.detected", self()), self());
		detectedPlates.clear();
		context().stop(self());
	}
}
