package tkocsis.akka.lpr.persist;

import java.util.Arrays;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.CouchbaseCluster;

public class CouchbaseConnectionProvider {

	static CouchbaseCluster cc;
	static Bucket bucket;
	
	static {
		cc = CouchbaseCluster.create(Arrays.asList("127.0.0.1"));
		bucket = cc.openBucket("vidux");
	}
	
	public static Bucket getBucket() {
		return bucket;
	}
}
