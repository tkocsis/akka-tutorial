package tkocsis.akka.lpr.persist;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

public class MysqlConnectionProvider {
	final static String userName = "root";
    final static String password = "";
    final static String url = "jdbc:mysql://localhost:3306/lprdb";
    
    public static Connection getConnection() throws SQLException {
    	return DriverManager.getConnection(url, userName, password);
    }
    
    public static DSLContext getContext(Connection conn) {
    	return DSL.using(conn, SQLDialect.MYSQL);
    }
}
