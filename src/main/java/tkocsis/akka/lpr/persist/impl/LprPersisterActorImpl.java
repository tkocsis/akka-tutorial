package tkocsis.akka.lpr.persist.impl;

import java.util.ArrayList;
import java.util.List;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.dispatch.OnSuccess;
import akka.pattern.Patterns;
import akka.routing.ActorRefRoutee;
import akka.routing.RoundRobinRoutingLogic;
import akka.routing.Routee;
import akka.routing.Router;
import scala.concurrent.Future;
import tkocsis.akka.lpr.message.DetectedPlate;
import tkocsis.akka.lpr.persist.LprPersister;

public class LprPersisterActorImpl implements LprPersister {

	ActorSystem actorSystem;
	Router router;
	
	public LprPersisterActorImpl(ActorSystem actorSystem) {
		this.actorSystem = actorSystem;
		List<Routee> routeeList = new ArrayList<>();
		
		for (int i = 0; i < 5; i++) {
			ActorRef persisterWorkerRef = actorSystem.actorOf(Props.create(LprPersisterActorWorker.class), "lpr-persister-" + i);
			routeeList.add(new ActorRefRoutee(persisterWorkerRef));
			
			Future<Object> fut = Patterns.ask(persisterWorkerRef, "message", 1000);
			fut.onSuccess(new OnSuccess<Object>() {
				@Override
				public void onSuccess(Object arg0) throws Throwable {
					
				}
			}, actorSystem.dispatcher());
		}
		router = new Router(new RoundRobinRoutingLogic(), routeeList);
	}
	
	@Override
	public void saveLpr(DetectedPlate plate) {
		router.route(plate, ActorRef.noSender());
	}
}
