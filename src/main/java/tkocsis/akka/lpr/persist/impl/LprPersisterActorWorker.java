package tkocsis.akka.lpr.persist.impl;

import java.sql.Connection;
import java.util.UUID;

import org.bson.Document;
import org.jooq.DSLContext;

import com.couchbase.client.java.document.JsonDocument;
import com.couchbase.client.java.document.json.JsonObject;
import com.datastax.driver.core.Session;

import akka.actor.AbstractActor;
import akka.japi.pf.ReceiveBuilder;
import tkocsis.akka.LprUtils;
import tkocsis.akka.lpr.db.tables.Licenseplates;
import tkocsis.akka.lpr.db.tables.records.LicenseplatesRecord;
import tkocsis.akka.lpr.message.DetectedPlate;
import tkocsis.akka.lpr.persist.CassandraConnector;
import tkocsis.akka.lpr.persist.CouchbaseConnectionProvider;
import tkocsis.akka.lpr.persist.MongoDBConnection;
import tkocsis.akka.lpr.persist.MysqlConnectionProvider;

public class LprPersisterActorWorker extends AbstractActor {

	public LprPersisterActorWorker() {
		receive(ReceiveBuilder
				.match(DetectedPlate.class, this::onReceiveDetectedPlate)
				.matchAny(message -> unhandled(message))
				.build());
	}
	
	public void onReceiveDetectedPlate(DetectedPlate plate) {
//		saveJooq(plate);
//		saveCouchbase(plate);
//		saveCassandra(plate);
//		saveMongoDb(plate);
	}
	
	public void saveJooq(DetectedPlate plate) {
		try (Connection conn = MysqlConnectionProvider.getConnection()) {
			DSLContext context = MysqlConnectionProvider.getContext(conn);
			LicenseplatesRecord licensePlateRecord = context.newRecord(Licenseplates.LICENSEPLATES);
			licensePlateRecord.setPlate(plate.getPlate());
			licensePlateRecord.store();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void saveCouchbase(DetectedPlate plate) {
		JsonObject jsonObject = JsonObject.create();
		jsonObject.put("plate", plate.getPlate());
		jsonObject.put("timestamp", plate.getTimestamp());
		JsonDocument document = JsonDocument.create("plate-" + UUID.randomUUID(), jsonObject);
		CouchbaseConnectionProvider.getBucket().upsert(document);
	}
	
	public void saveCassandra(DetectedPlate plate) {
		Session session = CassandraConnector.getSession();
		session.execute("INSERT INTO vidux.plates (plate, timestamp) VALUES (?, ?)", plate.getPlate(), plate.getTimestamp());
	}
	
	public void saveMongoDb(DetectedPlate plate) {
		Document doc = new Document("plate", LprUtils.randomLicense()).append("timestamp", plate.getTimestamp());
		MongoDBConnection.getLprCollection().insertOne(doc);
	}
	
}
