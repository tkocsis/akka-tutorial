package tkocsis.akka.lpr.persist;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;

public class CassandraConnector {

	private static Cluster cluster;
	private static Session session;
	
	static {
		cluster = Cluster.builder().addContactPoint("localhost").build();
		session = cluster.connect();
	}
	
	public static Session getSession() {
		return session;
	}
}
