package tkocsis.akka.lpr.persist;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class MongoDBConnection {

	
	static MongoClient mongoClient;
	static MongoDatabase database; 
	
	static {
		mongoClient = new MongoClient("localhost");
		database = mongoClient.getDatabase("vidux");
	}
	
	public static MongoCollection<Document> getLprCollection() {
		return database.getCollection("plates");
	}
	
}
