package tkocsis.akka.lpr.persist;

import tkocsis.akka.lpr.message.DetectedPlate;

public interface LprPersister {

	public void saveLpr(DetectedPlate plate);
	
}
