package tkocsis.akka.lpr;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;

import akka.Done;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.PoisonPill;
import akka.actor.Props;
import tkocsis.akka.lpr.http.LprListenerActor;
import tkocsis.akka.lpr.message.DetectedPlateRequest;

public class LprLiveFeedSource {
	
	ActorRef listenerActor;
	
	public LprLiveFeedSource(ActorSystem actorSystem) {
		listenerActor = actorSystem.actorOf(Props.create(LprListenerActor.class));
	}
	
	public CompletableFuture<Optional<Object>> nextPlate() {
		CompletableFuture<Optional<Object>> nextElement = new CompletableFuture<>();
		listenerActor.tell(new DetectedPlateRequest(nextElement), ActorRef.noSender());
		return nextElement;
	}
	
	public CompletableFuture<Done> close() {
		listenerActor.tell(PoisonPill.getInstance(), ActorRef.noSender());
		return CompletableFuture.completedFuture(Done.getInstance());
	}
}