package tkocsis.akka.lpr;

import akka.actor.ActorSystem;
import akka.actor.Props;
import kamon.Kamon;
import tkocsis.akka.lpr.http.HttpServer;
import tkocsis.akka.lpr.persist.impl.LprPersisterActorImpl;

public class Main {

	public static void main(String[] args) {
		Kamon.start();
		ActorSystem system = ActorSystem.create("lpr-system");
		system.actorOf(Props.create(LprDetectActor.class, new LprPersisterActorImpl(system)), "lpr-detector");
		new HttpServer(system);
	}
}
