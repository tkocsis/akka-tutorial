package tkocsis.akka.lpr.graphql;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.SelectJoinStep;

import graphql.GraphQL;
import graphql.Scalars;
import graphql.schema.GraphQLList;
import graphql.schema.GraphQLObjectType;
import graphql.schema.GraphQLSchema;
import tkocsis.akka.lpr.db.tables.Licenseplates;
import tkocsis.akka.lpr.db.tables.records.LicenseplatesRecord;
import tkocsis.akka.lpr.persist.MysqlConnectionProvider;

public class GraphQLService {
	
	GraphQL graphQL;
	
	public GraphQLService() {

		GraphQLObjectType licensePlates = GraphQLObjectType.newObject()
				.name("licensePlates")
				.field(f -> f.name("plate").type(Scalars.GraphQLString))
				.field(f -> f.name("timestamp").type(Scalars.GraphQLLong).dataFetcher(env -> {
					return ((LicenseplatesRecord) env.getSource()).getCreatedAt().getTime();
				}))
				.field(f -> f.name("date").type(Scalars.GraphQLString).dataFetcher(env -> {
					return ((LicenseplatesRecord) env.getSource()).getCreatedAt().toString();
				}))
				.build();
		
		GraphQLObjectType query = GraphQLObjectType.newObject()
				.name("query")
				.field(f -> f.name("lprs")
							.argument(arg -> arg.name("limit").type(Scalars.GraphQLInt).description("Limit of the lprs, default 10"))
							.argument(arg -> arg.name("search").type(Scalars.GraphQLString).description("Query string for like"))
							.type(new GraphQLList(licensePlates))
							.dataFetcher(env -> {
								List<LicenseplatesRecord> result = new ArrayList<>();
								try (Connection conn = MysqlConnectionProvider.getConnection()) {
									DSLContext context = MysqlConnectionProvider.getContext(conn);
									
									SelectJoinStep<Record> q = context.select().from(Licenseplates.LICENSEPLATES);
									if (env.getArgument("search") != null) {
										q.where(Licenseplates.LICENSEPLATES.PLATE.like("%" + env.getArgument("search") + "%"));
									}
									
									int limit = 10;
									if (env.getArgument("limit") != null) {
										limit = (int) env.getArgument("limit");
									}
									q.limit(limit);
									result.addAll(q.fetchInto(LicenseplatesRecord.class));
								} catch (Exception e) {
									e.printStackTrace();
								}
								return result;
							})
						)
				.build();
		
		GraphQLSchema schema = GraphQLSchema.newSchema().query(query).build();
		graphQL = new GraphQL(schema);
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> query(String query) {
		return (Map<String, Object>) graphQL.execute(query).getData();
	}
	
	public static void main(String[] args) {
		GraphQLService service = new GraphQLService();
		System.out.println(service.query("{ lprs(search: \"IBI\") { plate date } }"));
	}
	
}
