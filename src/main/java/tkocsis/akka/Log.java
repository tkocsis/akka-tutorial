package tkocsis.akka;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class Log {

	public static void log(String msg) {
		System.err.println("## " + ZonedDateTime.now().format(DateTimeFormatter.ISO_INSTANT) + " " 
				+ Thread.currentThread().getId() + " " + Thread.currentThread().getName() 
				+ ": " + msg);
	}
}
