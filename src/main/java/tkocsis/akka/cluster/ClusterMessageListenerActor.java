package tkocsis.akka.cluster;

import akka.actor.UntypedActor;
import akka.cluster.Cluster;
import akka.cluster.ClusterEvent.CurrentClusterState;
import akka.cluster.ClusterEvent.MemberEvent;
import akka.cluster.ClusterEvent.MemberRemoved;
import akka.cluster.ClusterEvent.MemberUp;
import akka.cluster.ClusterEvent.UnreachableMember;
import tkocsis.akka.Log;

public class ClusterMessageListenerActor extends UntypedActor {

	Cluster cluster = Cluster.get(getContext().system());

	@Override
	public void preStart() throws Exception {
		super.preStart();
		cluster.subscribe(getSelf(), MemberEvent.class, UnreachableMember.class);
	}

	@Override
	public void onReceive(Object message) throws Throwable {
		if (message instanceof CurrentClusterState) {
			CurrentClusterState state = (CurrentClusterState) message;
			Log.log("-------- Current members: " + state.members());

		} else if (message instanceof MemberUp) {	
			MemberUp mUp = (MemberUp) message;
			Log.log("-------- Member is Up: " + mUp.member());
			Log.log("-------- " + mUp.member().address());

		} else if (message instanceof UnreachableMember) {
			UnreachableMember mUnreachable = (UnreachableMember) message;
			Log.log("-------- Member detected as unreachable: " + mUnreachable.member());

		} else if (message instanceof MemberRemoved) {
			MemberRemoved mRemoved = (MemberRemoved) message;
			Log.log("-------- Member is Removed: " + mRemoved.member());

		} else if (message instanceof MemberEvent) {
			// ignore

		} else {
			unhandled(message);
		}
	}

}
