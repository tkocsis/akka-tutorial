package tkocsis.akka.cluster;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import akka.actor.ActorSystem;
import akka.actor.Props;
import tkocsis.akka.firstapp.LprListenerActor;

public class MainClusterNode {

	public static void main(String[] args) {
		Config config = ConfigFactory.parseString(
		          "akka.remote.netty.tcp.port=" + 0).withFallback(
		          ConfigFactory.load());
		
		ActorSystem system = ActorSystem.create("ClusterSystem");
		system.actorOf(Props.create(LprListenerActor.class), "lpr-listener-1");
		system.actorOf(Props.create(LprListenerActor.class), "lpr-listener-2");
		system.actorOf(Props.create(LprListenerActor.class), "lpr-listener-3");
		system.actorOf(Props.create(LprListenerActor.class), "lpr-listener-4");
	}
}
