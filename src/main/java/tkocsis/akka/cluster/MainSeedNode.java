package tkocsis.akka.cluster;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import akka.actor.ActorSystem;
import akka.actor.Props;
import tkocsis.akka.firstapp.LprDetectActor;

public class MainSeedNode {

	public static void main(String[] args) {
		Config config = ConfigFactory.parseString(
		          "akka.remote.netty.tcp.port=" + 2551).withFallback(
		          ConfigFactory.load());
		
		ActorSystem system = ActorSystem.create("ClusterSystem", config);
		system.actorOf(Props.create(ClusterMessageListenerActor.class), "message-listener");
		system.actorOf(Props.create(LprDetectActor.class), "lpr-detect");
	}
	
}

