package tkocsis.akka;

import java.util.Random;

public class LprUtils {

	public static final String LPR_SERVICE_NAME = "lpr-detect-service";
	public static final String LPR_CHANNEL = "lpr-detected";
	
	
	public static String randomLicense() {
		String part1 = randomChars("ABCDEFGHIJKLMNOPRST".toCharArray(), 3);
		String part2 = randomChars("123456789".toCharArray(), 3);
		return part1 + "-" + part2;
	}
	
	private static String randomChars(char[] chars, int length) {
		StringBuilder sb = new StringBuilder();
		Random random = new Random();
		for (int i = 0; i < length; i++) {
			char c = chars[random.nextInt(chars.length)];
			sb.append(c);
		}
		return sb.toString();
	}
}
